    	$(function () {
  			$('[data-toggle="tooltip"]').tooltip();
  			$('[data-toggle="popover"]').popover();
  			$('.carousel').carousel({
			  interval: 4000
			});

		});
		$('.modal').on('show.bs.modal', function(e){
				console.log('El modal se esta abriendo');	

				$('#contactoBtn').removeClass('btn-outline-success');
				$('#contactoBtn').addClass('btn-primary');
				$('#contactoBtn').prop({
					disabled: 'true'
				});

		});

		$('.modal').on('hidden.bs.modal', function(e){
				console.log('El modal se cerro');	

				
				let ContactoBoton=$('#contactoBtn');
				setTimeout(enableContactBtn(ContactoBoton),10000);

		});

		function enableContactBtn(element){
			debugger;
			element.removeClass('btn-primary');
			element.addClass('btn-outline-success');
			element.prop("disabled", false);
		}
